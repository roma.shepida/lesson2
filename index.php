<?php

echo 'task 1' . "<br/>"; // задача 1
$name = "Bob";
$age = 20;
echo "Hello $name! You are $age years old" . "<br/>";
echo 'Hello ' . $name . '! You are ' . $age . ' years old' . "<br/>";
echo "<br/>";

echo 'task 2' . "<br/>"; // задача 2
$x = 24;
$y = 18;
$sum = $x + $y;
echo "The sum of $x and $y is $sum" . "<br/>";
echo "<br/>";

echo 'task 3' . "<br/>"; // задача 3
$a = 42;
$b = 3.14;
$c = "string in task 3";
echo "integer a = $a" . "<br/>";
echo "float b = $b" . "<br/>";
echo "string c = $c" . "<br/>";
echo "<br/>";

echo 'task 4' . "<br/>"; // задача 4
$admin; //объявили переменную admin
$name; //объявили переменную name
$name = "Василий"; // присвоили переменной name строку "Василий"
$admin = $name; // присвоили переменной admin значение переменной name
echo "$admin";
echo "<br/>";
echo "<br/>";

echo 'task 5' . "<br/>"; // задача 5
$planetName = "Земля";
$visitorName = "Петя";
echo "<br/>";

echo 'task 6' . "<br/>"; // задача 6
//$a = 2;
//$x = 1 + (a *= 2); // выдаст ошибку, так как разные типы в значении Х, а нужно вызывать $a

echo "<br/>";

echo 'task 7 (if-else)' . "<br/>"; // задача 7
// пункт а)
$target = "55";
if ($target == 55) {
    echo "Верно";
} else {
    echo "Неверно";
}
echo "<br/>";
// пункт b)
if ($target === 55) {
    echo "Верно";
} else {
    echo "Неверно";
}
echo "<br/>";
// пункт c)
$lowTarget = 4;
if ($lowTarget <= 5) {
    echo "Меньше или равно пять";
} else {
    echo "Неверно";
}
echo "<br/>";
// пункт d)
$minute = 42;
if ($minute < 1 || $minute > 60) {
    echo "значение вне диапазона 1-60";
} else {
    if ($minute >=1 && $minute <=15) {
        echo "Первая четверть часа";
    } elseif ($minute >=16 && $minute <=30) {
        echo "Вторая четверть часа";
    } elseif ($minute >=31 && $minute <=45) {
        echo "Третья четверть часа";
    } else {
        echo "Четвертая четверть часа";
    }
}
echo "<br/>";
// пункт e)
$x = 10;
$y = 2;
if ( ($x <= 3 || $x > 10) && ($y >=2 && $y < 24)) {
    echo "True";
} else {
    echo "False";
}
echo "<br/>";
// пункт f)
$s = "34151";
$s1 = $s/10000;
$s2 = $s/1000%10;
$s3 = $s%1000/100;
$s4 = $s%100/10;
$s5 = $s%10;
if ( ( (int)$s1 + (int)$s2 ) === ( (int)$s3 + (int)$s4 + (int)$s5 ) ) {
    echo "да";
} else {
    echo "нет";
}
echo "<br/>";
echo "<br/>";

echo 'task 8' . "<br/>"; // задача 8 ... которую не нужно делать
$language = "ru_RU";
// через 2 if
if ($language === "en_EN") {
    $months = [
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December'
    ];
}
if ($language === "ru_RU") {
    $months = [
        'Январь',
        'Февраль',
        'Март',
        'Апрель',
        'Май',
        'Июнь',
        'Июль',
        'Август',
        'Сентябрь',
        'Октябрь',
        'Ноябрь',
        'Декабрь'
    ];
}
echo $months[0];
echo "<br/>";
// через многомерный массив
$months = [
    'en_EN' => [
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December'
    ],
    'ru_RU' => [
        'Январь',
        'Февраль',
        'Март',
        'Апрель',
        'Май',
        'Июнь',
        'Июль',
        'Август',
        'Сентябрь',
        'Октябрь',
        'Ноябрь',
        'Декабрь'
    ],
];
echo $months[$language][4];
echo "<br/>";
// через switch-case
echo "и тут я вспомнил что 8 задачу по домашке делать не нужно *рука-лицо";
echo "<br/>";
echo "<br/>";

echo 'task 9' . "<br/>"; // задача 9
$arr = ['World', '!', 'Hello'];
//echo "$arr[2]" . " " . "$arr[0]" . "$arr[1]"; // способ 1
echo "$arr[2] $arr[0]$arr[1]"; // способ 2
echo "<br/>"; echo "<br/>";

echo 'task 10' . "<br/>"; // задача 10
$responseStatuses = [
    '100' => 'Сообщения успешно приняты к отправке',
    '105' => 'Ошибка в формате запроса',
    '110' => 'Неверная авторизация',
    '115' => 'Недопустимый IP-адрес отправителя',
];
echo "$responseStatuses[105] $responseStatuses[110]";
echo "<br/>"; echo "<br/>";

echo 'task 11' . "<br/>"; // задача 11
$fruits = [
    'apple' => 3,
    'orange' => 5,
    'pear' => 10
];
echo $fruits['apple'] + $fruits['orange']; echo "<br/>";
echo $fruits['orange'] + $fruits['pear'];
echo "<br/>"; echo "<br/>";

echo 'task 12' . "<br/>"; // задача 12
$arr = [
    'cms' => [
        'joomla',
        'wordpress',
        'drupal'
    ],
    'colors' => [
        'blue'=>'голубой',
        'red'=>'красный',
        'green'=>'зеленый'
    ]
];
echo $arr['colors']['red'] . ", " . $arr['cms']['1']; echo "<br/>";
echo $arr['colors']['green'] . "<br/>" . $arr['cms']['2'];
